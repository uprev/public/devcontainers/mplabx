MPLABX Devcontainer
===================


This repo contains the dockerfile and devcontainer configuration for `uprev/mplabx <hub.docker.com/uprev/mplabx>`_

This image is based on ubuntu 22.04 and installs MPLABX as well as the XC8, XC16, and XC32 compilers.

The toolchain versions can be adjusted with the following build args: 

.. code:: dockerfile

    ARG MPLABX_VERSION=5.45
    ARG XC8_VERSION=2.45
    ARG XC16_VERSION=2.10
    ARG XC32_VERSION=4.35


Using the container 
-------------------

Once a project is mounted into the container, you can build it with the following command:

.. code:: bash

    /opt/mplabx/mplab_platform/bin/prjMakefilesGenerator.sh  .@default  #Create makefile from project in the current directory using the default build config 
    make                                                                #run the build
    
.. note:: A template for the Gitlab CI script can be found here: https://gitlab.com/uprev/public/templates/ci-templates/-/raw/main/mplabx.gitlab-ci.yml