FROM uprev/base:ubuntu-22.04 as dev 

ARG MPLABX_VERSION=5.45
ARG XC8_VERSION=2.45
ARG XC16_VERSION=2.10
ARG XC32_VERSION=4.35
ARG PIC18F_J_DFP_VERSION=1.7.159


# Install the dependencies
# See https://microchipdeveloper.com/install:mplabx-lin64
RUN dpkg --add-architecture i386 \
  && apt-get update \
  && apt-get install -y --no-install-recommends \
    libc6:i386 \
    libx11-6:i386 \
    libxext6:i386 \
    libstdc++6:i386 \
    libexpat1:i386 \
    ca-certificates \
    sudo \
  && apt-get clean \
  && apt-get autoremove \
  && rm -rf /var/lib/apt/lists/*

# Download and install XC8
RUN wget -nv -O /tmp/xc8 "https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/SoftwareTools/xc8-v${XC8_VERSION}-full-install-linux-x64-installer.run" \
  && chmod +x /tmp/xc8 \
  && /tmp/xc8 --mode unattended --unattendedmodeui none --netservername localhost --LicenseType FreeMode --prefix "/opt/microchip/xc8/v${XC8_VERSION}" \
  && rm /tmp/xc8

RUN wget -nv -O /tmp/xc16 "https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/SoftwareTools/xc16-v${XC16_VERSION}-full-install-linux64-installer.run" \
  && chmod +x /tmp/xc16 \
  && /tmp/xc16 --mode unattended --unattendedmodeui none --netservername localhost --LicenseType FreeMode --prefix "/opt/microchip/xc16/v${XC16_VERSION}" \
  && rm /tmp/xc16

# Download and install XC32
RUN wget -nv -O /tmp/xc32.tar "https://ww1.microchip.com/downloads/aemDocuments/documents/DEV/ProductDocuments/SoftwareTools/xc32-v${XC32_VERSION}-full-install-linux-x64-installer.tar" \ 
    && tar -xf /tmp/xc32.tar -C /tmp/ \ 
    && mv /tmp/xc32-v${XC32_VERSION}-full-install-linux-x64-installer.run /tmp/xc32 \
    && rm /tmp/xc32.tar \
    && chmod +x /tmp/xc32 \
    && /tmp/xc32 --mode unattended --unattendedmodeui none --netservername localhost --LicenseType FreeMode --prefix "/opt/microchip/xc32/v${XC32_VERSION}" \
    && rm /tmp/xc32


#download and install PIC18F_J_DFP
RUN wget -nv -O /tmp/PIC18F_J_DFP.atpack "https://packs.download.microchip.com/Microchip.PIC18F-J_DFP.${PIC18F_J_DFP_VERSION}.atpack" \ 
    && mkdir -p /opt/mplabx/packs/Microchip/PIC18F_J_DFP \
    && unzip /tmp/PIC18F_J_DFP.atpack -d /opt/mplabx/packs/Microchip/PIC18F_J_DFP/${PIC18F_J_DFP_VERSION} \
    && rm /tmp/PIC18F_J_DFP.atpack 


# Download and install MPLAB X
# hadolint ignore=DL3004
RUN wget -nv -O /tmp/mplabx.tar "https://ww1.microchip.com/downloads/en/DeviceDoc/MPLABX-v${MPLABX_VERSION}-linux-installer.tar" \
  && tar -xf /tmp/mplabx.tar -C /tmp/  \
  && rm /tmp/mplabx.tar \
  && mv /tmp/MPLABX-v${MPLABX_VERSION}-linux-installer.sh /tmp/mplabx \
  && sudo /tmp/mplabx --nox11 -- --unattendedmodeui none --mode unattended --ipe 0 --collectInfo 0 --installdir /opt/mplabx --16bitmcu 0 --32bitmcu 0 --othermcu 0 \
  && rm /tmp/mplabx

